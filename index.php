<?php
    require_once('header.php');
?>
    <h3>Список таблиц базы данных <?php echo $config['db']; ?> </h3>
    <table class="table">
        <tr>
            <td>Название таблицы</td>
            <td></td>
        </tr>
<?php        
    foreach($results as $result=>$value)
        {
           $tName = $value['Tables_in_'.$config['db']];
           $params = array_merge($_GET, array("table_name" => "$tName"));
           $new_query_string = http_build_query($params); 
?>         
        <tr>
            <td><?php echo $tName; ?></td>
            <td><a href="<?php echo 'details.php?'.$new_query_string ;?>">Перейти к таблице</a></td>
        </tr>
  <?php      }?>
    </table>
<?php
    require_once('footer.php');
?>


