<?php
namespace DBManager; 

class DBManager
{
	protected $host;
	protected $user;
	protected $pass;
	protected $db;
	protected $dbHandler; 
	
	function __construct($host, $user, $pass, $db)
	{
		$this->host = $host;
		$this->user =  $user;
		$this->pass = $pass;
		$this->db = $db;
		$this->doConnect();
	}
	
	protected function doConnect()
	{
		try
		{
			$this->dbHandler = new \PDO('mysql:host='.$this->host.';dbname='.$this->db, $this->user, $this->pass);
		 	$this->dbHandler->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
		 	$this->dbHandler->exec("set names utf8");
	 	}
	    catch (PDOException $e) 
	    {
			 print "Error!: " . $e->getMessage() . "<br/>";
			 die();
		}
	}
	
	public function getDbHandler()
	{
		$this->doConnect();
		return $this->dbHandler;
	}
	
	public function showTables()
	{
		$sth = $this->dbHandler->prepare('SHOW TABLES');
	    $sth->execute();
	    return $sth->fetchAll();
	}
	
	public function changeColumnName($tableName, $oldColumnName, $newColumnName, $columnType)
	{
			$stmt = $this->dbHandler->prepare("ALTER TABLE $tableName CHANGE $oldColumnName  $newColumnName $columnType");
			$stmt->execute();
	    	return $stmt;
	}
	
	public function changeColumnType($tableName, $columnName, $columnType){
			$stmt = $this->dbHandler->prepare("ALTER TABLE $tableName MODIFY $columnName $columnType");
			$stmt->execute();
	    	return $stmt;
	}
	
	public function showTableDetails($tableName)
	{
		 $sql = "DESCRIBE $tableName";
	     $query = $this->dbHandler->query($sql);
	     $q = $query->fetchAll(\PDO::FETCH_COLUMN);
	   	 return  $q;
	}
	
	public function getColumnMeta($tableName)
	{
		 $sql = "DESCRIBE $tableName";
	     $query = $this->dbHandler->query($sql);
	     $results = $query->fetchAll();
	     return $results; 
	}

	public function createTestTables()
	{
		$this->createTable1();
		$this->createTable2();
	}
	
	public function dropTestTables()
	{
		try {
			  $sql ="DROP TABLE IF EXISTS `table1`";
			  $res = $this->dbHandler->exec($sql);
		 	} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
		try {
			  $sql ="DROP TABLE IF EXISTS `table2`";
			  $res = $this->dbHandler->exec($sql);
		 	} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
	
	protected function createTable1()
	{
		try {
			 $sql ="CREATE TABLE IF NOT EXISTS `table1` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `user_id` int(11) NOT NULL,
				  `assigned_user_id` int(11) DEFAULT NULL,
				  `description` text NOT NULL,
				  `is_done` tinyint(4) NOT NULL DEFAULT '0',
				  `date_added` datetime NOT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";	
			 $res = $this->dbHandler->exec($sql);
		 	} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}
	
	protected function createTable2()
	{
		try {
			 $sql ="CREATE TABLE IF NOT EXISTS `table2`(
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `login` varchar(50) NOT NULL,
			  `password` varchar(255) NOT NULL,
			  PRIMARY KEY (`id`)
			  )  ENGINE=InnoDB DEFAULT CHARSET=utf8";	
			 $res = $this->dbHandler->exec($sql);
		 	} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}
	}

}