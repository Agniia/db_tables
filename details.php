<?php
    require_once('header.php');
         
    if(isset($_GET['table_name']))
    {
        $tableName = strip_tags($_GET['table_name']);
        $table_fields = $dbInstance->getColumnMeta($tableName);
    } 
   if(!empty($_POST['old_column_name']) &&
   !empty($_POST['new_column_name']) &&
   !empty($_POST['column_type']) )
    {
       header('Location: index.php');
        $tableName = strip_tags($_GET['table_name']);
        $oldColumnName = strip_tags($_POST['old_column_name']);
        $newColumnName = strip_tags($_POST['new_column_name']);
        $columnType = strip_tags($_POST['column_type']);
        $dbInstance->changeColumnName($tableName, $oldColumnName, $newColumnName, $columnType);
    } 
    else if(!empty($_POST['column_name']) && !empty($_POST['column_type']) )
    {
       header('Location: index.php');
        $tableName = strip_tags($_GET['table_name']);
        $columnName = strip_tags($_POST['column_name']);
        $columnType = strip_tags($_POST['column_type']);
        $dbInstance->changeColumnType($tableName, $columnName, $columnType);
    }
    ?>
    <h3>Таблица <?php echo $tableName; ?> </h3>
    <table class="table">
         <tr>
            <td>Название столбца</td>
             <td>Тип столбца</td>
         </tr>
        <?php  foreach($table_fields as $key=>$value): ?>
                <?php  if(!isset($value) || empty($value)): continue; endif; ?>
                <?php  if($tableName == 'tasks' || $tableName == 'users'): ?>       
                    <tr>
                        <td><?php echo $value['Field']; ?></td>
                        <td><?php echo $value['Type']; ?> </td>
                    </tr>
                <?php  else: ?>
                    <tr>
                        <td>
                            <form method="post">
                                <input type="hidden" name="old_column_name" value="<?php echo $value['Field'];  ?>">  
                                <input type="hidden" name="column_type" value="<?php echo $value['Type']; ?>">  
                                <input type="text" name="new_column_name" value="<?php echo $value['Field']; ?>">      
                                <button type="submit" class="btn btn-primary">Изменить</button>                  
                            </form>
                        </td>
                        <td>       
                            <form method="post">
                                <input type="hidden" name="column_name" value="<?php echo $value['Field'];  ?>">  
                                <input type="text" name="column_type" value="<?php echo $value['Type']; ?>">      
                                <button type="submit" class="btn btn-primary">Изменить</button>                  
                            </form>
                        </td>
                    </tr>
                <?php  endif; ?>
             <?php  endforeach; ?>
    </table>
<?php
    require_once('footer.php');
?>