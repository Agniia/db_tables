<?php
    header('Content-Type: text/html; charset=utf-8');
     function myAutoloader($className)
    {
       $rootDir = str_replace('page_components','',__DIR__);               
       $file = str_replace('\\',DIRECTORY_SEPARATOR,$className);
       $fileName = $rootDir .'/classes'.DIRECTORY_SEPARATOR . $file . '.php'; 
       if(file_exists($fileName))
        {
    		include $fileName;
        }
    }
    spl_autoload_register('myAutoloader');         
    $config = include 'config.php';      
    $dbInstance = new \DBManager\DBManager($config['host'], $config['user'], $config['pass'], $config['db']);
    //$dbInstance->dropTestTables();
    $dbInstance->createTestTables();
    $results = $dbInstance->showTables();
    ?>
    <!DOCTYPE html>
    <html>
     <head>
       <title>DB tables</title>
       <meta charset="utf-8">
       <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <style>
            .main-cont{
                width: 80%;
                margin: 30px auto;
            }
            h3{
                  margin-bottom: 30px 
            }
        </style>
     </head>
     <body>
     <nav class="navbar navbar-expand-lg navbar-light bg-light">
         <a class="navbar-brand" href="index.php">Главная</a>
     </nav>
     <div class="main-cont">